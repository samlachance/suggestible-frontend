// Libs
import Vue from 'vue'
import Vuex from 'vuex'
import 'bootstrap';
Vue.use(Vuex)

// Routes
import router from './router'

// Styles
import 'sanitize.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import './assets/styles/main.scss';

// Config
Vue.config.debug         = process.env.NODE_ENV == 'development'
Vue.config.devtools      = process.env.NODE_ENV == 'development'
Vue.config.productionTip = process.env.NODE_ENV == 'development'
Vue.config.silent        = process.env.NODE_ENV != 'development'

// Vue
import App from './components/App.vue'

new Vue({
  el: '#app',
  router,
  render: h => h(App)
});

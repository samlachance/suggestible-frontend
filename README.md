# Suggestible front-end (sinatra/vue)

## Commands

### Using Make

Setup

```
make setup
```

Build

```
make build
```

Run webpack server

```
make webpack
```

Run Sinatra server

```
make sinatra
```

Build and run Sinatra

```
make prod
```

### Using native commands

#### Develop

Install npm packages

```
npm install
```

Run webpack server

```
npm run dev
```

Build with webpack

```
npm run build
```

#### Run

Install gems

```
bundle install
```

Run sinatra

```
rackup
```